const nodemailer = require('nodemailer');

const mailConnection = {
    init() {
        return new Promise((resolve, reject) => {
            nodemailer.createTestAccount((err, account) => {
                if(err !== null)
                    return reject(err);

                // create reusable transporter object using the default SMTP transport
                this.transporter = nodemailer.createTransport({
                    host: 'smtp.ethereal.email',
                    port: 587,
                    secure: false, // true for 465, false for other ports
                    auth: {
                        user: account.user, // generated ethereal user
                        pass: account.pass // generated ethereal password
                    }
                });

                resolve(true);
            });
        });
    },

    sendMail(options) {
        return new Promise((resolve, reject) => {
            if(this.transporter === void 0)
                return reject('Transported is undefined');
            
            this.transporter.sendMail(options, (err, info) => {
                if(err !== null)
                    return reject(err);

                const url = nodemailer.getTestMessageUrl(info);
                
                console.log('Message sent: %s', info.messageId);
                console.log('Preview URL: %s', url);

                resolve(url)
            });
        });
    },
};

module.exports = mailConnection;