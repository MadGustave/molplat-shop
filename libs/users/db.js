const dbConnection = require('../db_connection.js');
const config = require('../../config.json');
const utils = require('../utils.js');
const crypto = require('crypto');
const UserRoles = require('./UserRoles.js');
const ObjectID = require('mongodb').ObjectID;

/**
 * collection:
 * email: <string>
 * passHash: <string>
 * passSalt: <string>
 * regTime: <timestamp>
 * sessionToken: <string>
 * roles: <int>
 * ban_until: <timestamp>
 * 
 * 
 ** only unfinished reg. fields
 * finishRegToken: <string>
 *
 ** only restoring password fields
 * restorePassToken: <string>
 */

// TODO: catch errors(and validate?)

// remove throw

let db, usersCollection;

dbConnection.init()
.then(() => {
	db = dbConnection.client.db(config.db.main_db_name);
	usersCollection = db.collection(config.users.collection_name);
});

exports.finishRegistration = (regToken) => {
	return usersCollection.findOneAndUpdate(
		{ finishRegToken: regToken },
		{
			$unset: { finishRegToken: "" }
		},
	)
	.then((result) => {
		if(result.value === null)
			throw new Error('Update operation did not update anything');

		return result.value._id;
	});
};

// return mongodb document id
exports.initRegistration = (email, pass) => {
	// TODO: add validation here or to collection
	const hash = crypto.createHash('sha512');

	const passSalt = utils.genRandomString(config.users.salt_length);

	hash.update(passSalt);
	hash.update(pass);

	const passHash = hash.digest('hex');
	const finishRegToken = utils.genRandomString(config.users.reg_token_length);

	return usersCollection.insertOne({
		email,
		passHash,
		passSalt,
		regTime: Date.now(),
		finishRegToken,
	})
	.then((insertResult) => {
		if(insertResult.insertedCount !== 1)
			throw new Error('New user was not inserted for some reason');

		return [insertResult.insertedId, finishRegToken];
	});
};

exports.authUser = (email, pass) => {
	// TODO validation
	return usersCollection.findOne({
		email,
		finishRegToken: { $exists: false },
	})
	.then((doc) => {
		if(doc === null)
			throw new Error('No registered user with such email found');

		// check pass
		const hash = crypto.createHash('sha512');

		hash.update(doc.passSalt);
		hash.update(pass);

		const passHash = hash.digest('hex');

		if(passHash !== doc.passHash)
			throw new Error('Password hash is not correct');

		// update session token
		const sessionToken = utils.genRandomString(config.users.session_token_length);

		return Promise.all([usersCollection.updateOne(
			{ _id: doc._id },
			{
				$set: { sessionToken },
			},
		), sessionToken]);
	})
	.then(([updateResult, token]) => {
		if(updateResult.modifiedCount !== 1)
			throw new Error('Session token was not saved');

		return token;
	});
};

// TODO: do one function with different types 'by'
/*exports.getUserId = (email) => {

};*/
exports.getUserIdBySessionToken = (token) => {
	return usersCollection.findOne({
		sessionToken: token
	})
	.then((doc) => {
		if(doc === null)
			return null;

		return doc._id.toHexString();
	});
};

exports.getUserIdByEmail = (email) => {
	return usersCollection.findOne({
		email,
	})
	.then((doc) => {
		if(doc === null)
			return null;

		return doc._id.toHexString();
	});
};

exports.getUserIdByRestorePassToken = (token) => {
	return usersCollection.findOne({
		restorePassToken: token,
	})
	.then((doc) => {
		if(doc === null)
			return null;

		return doc._id.toHexString();
	});
};

// TODO
exports.setRestorePassToken = (id, value) => {
	let updateOperatorName = '$set';

	// removing this
	if(
		value === null
		|| value === void 0
		|| value === ''
	) {
		updateOperatorName = '$unset';

		value = '';
	}

	return usersCollection.findOneAndUpdate(
		{
			_id,
		},

		{
			[updateOperatorName]: {
				restorePassToken: value,
			},
		}
	)
	.then((result) => {
		if(result.ok === 1)
			return true;

		return false;
	})
	.catch((err) => {
		console.log('[ERROR] libs/users/db - setRestorePassToken', id, value, err);

		return false;
	});
};

exports.changeUserPassword = (id, pass) => {
	const hash = crypto.createHash('sha512');

	const passSalt = utils.genRandomString(config.users.salt_length);

	hash.update(passSalt);
	hash.update(pass);

	const passHash = hash.digest('hex');

	return usersCollection.findOneAndUpdate(
		{
			_id,
		},

		{
			$set: {
				passHash,
				passSalt,
			},
		}
	)
	.then((result) => {
		if(result.ok === 1)
			return true;

		return false;
	})
	.catch((err) => {
		console.log('[ERROR] libs/users/db - changeUserPassword', id, pass, err);

		return false;
	});
};

exports.getUserRoles = (id) => {
	const _id = new ObjectID(id);

	return usersCollection.findOne(
		{ 
			_id,
		},

		{
			fields: {
				'roles': 1,
			}
		}
	)
	.then((result) => {
		if(result === null)
			return null;

		if(result instanceof Object === true && Number.isInteger(result.roles) === false)
			return null;
	
		return new UserRoles(result.roles);
	})
	.catch((err) => {
		console.error('[ERROR] libs/users/db.js - getUserRoles', id, err);
	
		return null;
	});
};

exports.setUserBan = (id, timestamp) => {
	const _id = new ObjectID(id);

	return usersCollection.findOneAndUpdate(
		{
			_id,
		},

		{
			$set: {
				'ban_until': timestamp,
			}
		}
	)
	.then((result) => {
		if(result.ok === 1 && result.value !== null)
			return true;

		return false;
	})
	.catch((err) => {
		console.error('[ERROR] libs/users/db.js - setUserBan', id, err);

		return false;
	});
};

exports.getUserBan = (id) => {
	const _id = new ObjectID(id);

	return usersCollection.findOne(
		{ 
			_id,
		},

		{
			fields: {
				'ban_until': 1,
			}
		}
	)
	.then((result) => {
		if(result === null)
			return null;

		if(result instanceof Object === true && Number.isInteger(result.ban_until) === false)
			return null;
	
		return result.ban_until;
	})
	.catch((err) => {
		console.error('[ERROR] libs/users/db.js - getUserBan', id, err);
	
		return null;
	});
};

