const express = require('express');
const usersDb = require('./db.js');
const validator = require('validator');
const config = require('../../config.json');
const mailConnection = require('../mail_connection.js');
const ObjectID = require('mongodb').ObjectID;
const rolesMiddleware = require('./roles_middleware.js');
const UserRoles = require('./UserRoles.js');
const utils = require('../utils.js');

const router = express.Router();
const adminMiddleware = rolesMiddleware(UserRoles.ADMIN_MASK);

// admin-only methods
const ADMIN_METHODS = [
	'/set_ban',
];

router.use(ADMIN_METHODS, adminMiddleware);

// signin(up) methods
router.post('/signup', async (req, res) => {
	// already signin
	if(ObjectID.isValid(req.userId) === true) {
		return res.status(400).json({
			success: false,
		});
	}

	const body = req.body;

	if(body instanceof Object === false) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	if(
		typeof body.pass !== 'string'
		|| typeof body.email !== 'string'
		|| validator.isAlphanumeric(body.pass) === false
		|| body.pass.length < config.users.min_pass_length
		|| body.pass.length > config.users.max_pass_length
		|| validator.isEmail(body.email) === false
	) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	body.email = body.email.toLowerCase();

	const userId = await usersDb.getUserIdByEmail(body.email);

	// email isn't free
	if(userId !== null)
		return res.status(400).end();
	
	usersDb.initRegistration(body.email, body.pass)
	.then(([userId, regToken]) => {
		return mailConnection.sendMail({
	        from: '"MolAdmin 👻" <admin@moltest.com>',
	        to: body.email,
	        subject: 'Finish registration',
	        text: `Token: ${regToken}`,
    	});
	})
	.then((url) => {
		// TODO: send real mail
		res.json({
			success: true,
			url,
		});
	})
	.catch((err) => {
		console.error('Some error occured during signup', body, err);
		
		res.status(400).json({
			success: false,
		});
	});
});

router.post('/signup_finish', (req, res) => {
	// already signin
	if(ObjectID.isValid(req.userId) === true) {
		return res.status(400).json({
			success: false,
		});
	}

	const body = req.body;

	if(body instanceof Object === false) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	if(
		typeof body.reg_token !== 'string'
		|| validator.isAlphanumeric(body.reg_token) === false
		|| body.reg_token.length !== config.users.reg_token_length
	) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	usersDb.finishRegistration(body.reg_token)
	.then((id) => {
		res.status(400).json({
			success: true,
		});
	})
	.catch((err) => {
		console.error('Some error occured during signup finish', body, err);
		
		res.status(400).json({
			success: false,
		});
	})
});

router.post('/signin', (req, res) => {
	// already signin
	if(ObjectID.isValid(req.userId) === true) {
		return res.status(400).json({
			success: false,
		});
	}

	const body = req.body;

	if(body instanceof Object === false) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	if(
		typeof body.pass !== 'string'
		|| typeof body.email !== 'string'
		|| validator.isAlphanumeric(body.pass) === false
		|| body.pass.length < config.users.min_pass_length
		|| body.pass.length > config.users.max_pass_length
		|| validator.isEmail(body.email) === false
	) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	usersDb.authUser(body.email, body.pass)
	.then((token) => {
		// TODO: make this http-only, secure and signed?(make more secure)
		res.cookie('session_token', token);

		res.json({
			success: true,
		});
	})
	.catch((err) => {
		console.error('Some error occured during signin finish', body, err);
		
		res.status(400).json({
			success: false,
		});
	})
});

router.post('/signout', (req, res) => {
	// user not logged
	if(ObjectID.isValid(req.userId) === false)
		return res.status(400).end();

	req.userId = void 0;
	
	res.clearCookie('session_token');

	res.status(200).end();
});

router.post('/restore_pass', async (req, res) => {
	// already signin
	if(ObjectID.isValid(req.userId) === true) {
		return res.status(400).json({
			success: false,
		});
	}

	const body = req.body;

	if(body instanceof Object === false) {
		return res.status(400).json({
			success: false,
		});
	}

	if(
		typeof body.email !== 'string'
		|| validator.isEmail(body.email) === false
	) {
		return res.status(400).json({
			success: false,
		});
	}

	const email = body.email.toLowerCase();

	// check if there is account with that email
	const userId = await usersDb.getUserIdByEmail(email);

	// no such email found
	if(userId === null)
		return res.status(400).end();
	
	// generate token
	const restorePassToken = utils.genRandomString(40);

	// save it in db
	const tokenSaveResult = await usersDb.setRestorePassToken(userId, restorePassToken);
	
	if(tokenSaveResult === false)
		return res.status(400).end();

	// send email
	mailConnection.sendMail({
        from: '"no-reply 👻" <no-reply@moltest.com>',
        to: email,
        subject: 'Password restoring',
        text: `restorePassToken: ${restorePassToken}`,
	});

	res.status(200).end();
});

router.post('/change_password', async (req, res) => {
	const body = req.body;

	if(body instanceof Object === false)
		return res.status(400).end();

	// validate new pass instantly
	if(
		typeof body.pass !== 'string'
		|| validator.isAlphanumeric(body.pass) === false
		|| body.pass.length < config.users.min_pass_length
		|| body.pass.length > config.users.max_pass_length
	)
		return res.status(400).end();

	// should be logged(i.e. have userId field in req) or have restore password token
	let userId = void 0;
	let restoringPass = false;

	if(ObjectID.isValid(req.userId) === true)
		userId = req.userId;
	else {
		// validate input
		if(
			typeof body.restore_pass_token !== 'string'
			|| validator.isAlphanumeric(body.restore_pass_token) === false
		)
			return res.status(400).end();

		restoringPass = true;

		userId = await usersDb.getUserIdByRestorePassToken(body.restore_pass_token);
	}

	if(typeof userId !== 'string')
		return res.status(400).end();

	const changeResult = await usersDb.changeUserPassword(userId, body.pass);

	if(changeResult === false)
		return res.status(400).end();

	// if it was from restoring - remove restorePassToken
	if(restoringPass === true)
		usersDb.setRestorePassToken(userId, null);

	res.status(200).end();
});

// users control methods
router.post('/set_ban', (req, res) => {
	const body = req.body;

	if(body instanceof Object === false) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	if(
		ObjectID.isValid(req.user_id) === false
		|| Number.isInteger(req.ban_until) === false
	) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	usersDb.setUserBan(req.user_id, req.ban_until)
	.then((result) => {
		res.json({
			success: result,
		});
	});
});

module.exports = router;

