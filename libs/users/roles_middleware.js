const usersDb = require('./db.js');
const ObjectID = require('mongodb').ObjectID;

/**
 * middleware for only-roles entry
 * @param  {Int} rolesMask [description]
 * @return {Function}           [description]
 */
module.exports = (rolesMask) => {
	return async (req, res, next) => {
		if(rolesMask === 0)
			return next();

		if(ObjectID.isValid(req.userId) === false)
			return res.status(401).end();

		if(req.userRoles === null)
			return res.status(401).end();

		if(req.userRoles === void 0) {
			req.userRoles = await usersDb.getUserRoles(req.userId);

			if(req.userRoles === null)
				return res.status(401).end();
		}

		const hasRoles = !!(req.userRoles.roles & rolesMask);

		if(hasRoles === false)
			return res.status(401).end();
		else
			return next();
	};
};