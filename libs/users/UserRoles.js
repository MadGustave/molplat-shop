module.exports = class UserRoles {
	constructor(roles) {
		this.roles = roles;
	}

	isAdmin() {
		return !!(this.roles & UserRoles.ADMIN_MASK);
	}

	static get ADMIN_MASK() {
		return 0b1;
	}
}