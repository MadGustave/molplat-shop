const validator = require('validator');
const config = require('../../config.json');
const usersDb = require('./db.js');

module.exports = (req, res, next) => {
	req.userId = void 0;

	const token = req.cookies.session_token;

	if(
		typeof token !== 'string'
		|| validator.isAlphanumeric(token) === false
		|| token.length !== config.users.session_token_length
	) {
		return next(); 
	}

	usersDb.getUserIdBySessionToken(token)
	.then((id) => {
		if(id !== null)
			req.userId = id;

		next();
	})
	.catch((err) => {
		console.error('[ERROR] error occured while check request auth', token, err);

		next();
	});
};