const express = require('express');
const bodyParser = require('body-parser');

const itemsRouter = require('./items/api.js');
const usersRouter = require('./users/api.js');

const router = express.Router();

router.use(bodyParser.json());

router.use('/items', itemsRouter);
router.use('/users', usersRouter);

module.exports = router;