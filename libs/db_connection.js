const MongoClient = require('mongodb').MongoClient;
const config = require('../config.json');

const dbConnection = {
	init() {
		if(this.initPromise !== void 0)
			return this.initPromise;

		this.initPromise = MongoClient.connect(config.db.url,
		{ 
			useNewUrlParser: true,
		})
		.then((mongoClient) => {
			this.client = mongoClient;

			return true;
		});

		return this.initPromise;
	},
};

module.exports = dbConnection;