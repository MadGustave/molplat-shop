const crypto = require('crypto');

exports.randomSetValue = (set) => {
	const values = Array.from(set);

	return values[Math.floor(Math.random() * set.size)];
};

exports.genRandomString = (length) => {
    return crypto.randomBytes(Math.ceil(length/2))
    .toString('hex') /** convert to hexadecimal format */
    .slice(0,length);   /** return required number of characters */
};