const express = require('express');
const fs = require('fs');
const request = require('request');
const ObjectID = require('mongodb').ObjectID;
const path = require('path');
const utils = require('../utils');
const dbConnection = require('../db_connection.js');
const config = require('../../config.json');
const multer  = require('multer');
const UserRoles = require('../users/UserRoles.js');
const rolesMiddleware = require('../users/roles_middleware.js');

const router = express.Router();

module.exports = router;

const cwd = process.cwd();

const adminMiddleware = rolesMiddleware(UserRoles.ADMIN_MASK);

// admin-only methods
const ADMIN_METHODS = [
	'/add_item',
	'/update_item',
];

router.use(ADMIN_METHODS, adminMiddleware);

let itemsCollection;

dbConnection.init()
.then(() => {
	const db = dbConnection.client.db(config.db.main_db_name);

	itemsCollection = db.collection(config.items.collection_name);
});

// TODO: add validation

/**
 * db item structure
 *
 * price: <Int>
 * name: <String>
 * vendor: <Int>
 * category: <Int>
 * orig_url: <String>
 * preview_img: <ViewImg>
 * view_imgs: [<ViewImg>, <ViewImg>, ...]
 * specs: <Specs>
 * active: 0 | 1
 *
 * TODO:
 * last_update: <timestamp>
 *
 * <Specs>: {
 * 	spec_<spec_id>: <spec_value>
 * 	spec_<spec_id>: <spec_value>
 * 	...
 * }
 *
 * <ViewImg>: {
 * 	path: <String>
 * 	orig_basename: <String>
 * }
 */

// TODO: add orig_id

// TODO: save specs in another way(object)
router.post('/upsert_items', (req, res) => {
	const body = req.body;

	// TODO: whitelist by ip and something like password, only https
	// TODO: validate images
	// TODO: validate images URL
	
	if(
		body instanceof Object === false
		|| body.items instanceof Array === false
		|| body.items.length === 0
	) {
		res.status(400).json({
			success: false,
		});

		return;
	}

	const queryPromises = [];

	// insert/update items in db
	for(const reqItem of body.items) {
		// validate input
		if(
			Number.isInteger(reqItem.price) === false
			|| reqItem.price < 1
			|| typeof reqItem.name !== 'string'
			|| typeof reqItem.orig_url !== 'string'
			|| Number.isInteger(reqItem.vendor) === false
			|| Number.isInteger(reqItem.category) === false
			|| typeof reqItem.preview_img !== 'string'
			|| config.items.img_exts.indexOf(path.extname(reqItem.preview_img)) === -1
		) {
			queryPromises.push(Promise.resolve({
				success: false,
				origUrl: reqItem.orig_url,
				reason: 'input validation',
			}));

			continue;
		}

		// check if item with same orig_url already in db
		const dbResultPromise = itemsCollection.findOne(
			{
				orig_url: reqItem.orig_url,
			},

			{
				fields: {
					preview_img: 1,
					view_imgs: 1,
				}
			}
		)
		.then((dbResult) => {
			let returnPromise;

			// create object for db update operation
			const item = {
				price: reqItem.price,
				name: reqItem.name,
				vendor: reqItem.vendor,
				category: reqItem.category,
				orig_url: reqItem.orig_url,
				specs: reqItem.specs,
			};

			// images to download for this item and their local path
			const itemImgs = [];

			// add item
			if(dbResult === null) {
				const itemId = new ObjectID();
				const itemHexId = itemId.toHexString();

				// change mongoDB _id
				item._id = itemId;

				// preview image
				itemImgs.push({
					url: reqItem.preview_img,
					path: `${cwd}/public/img/items/${itemHexId}_preview${path.extname(reqItem.preview_img)}`
				});

				item.preview_img = {
					path: `/img/items/${itemHexId}_preview${path.extname(reqItem.preview_img)}`,
					orig_basename: path.basename(reqItem.preview_img),
				};

				// view images
				item.view_imgs = [];

				for(const url of reqItem.view_imgs) {
					if(config.items.img_exts.indexOf(path.extname(url)) === -1)
						continue;

					const basename = path.basename(url);
					const randomPostfix = utils.genRandomString(10);

					itemImgs.push({
						path: `${cwd}/public/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
						url,
					});

					item.view_imgs.push({
						path: `/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
						orig_basename: basename,
					});
				}

				returnPromise = itemsCollection.insertOne(item);
			}
			// update item
			else {
				const itemId = dbResult._id;
				const itemHexId = itemId.toHexString();

				// update preview image
				if(dbResult.preview_img.orig_basename !== path.basename(reqItem.preview_img)) {
					itemImgs.push({
						url: reqItem.preview_img,
						path: `${cwd}/public/img/items/${itemHexId}_preview${path.extname(reqItem.preview_img)}`
					});

					item.preview_img = {
						path: `/img/items/${itemHexId}_preview${path.extname(reqItem.preview_img)}`,
						orig_basename: path.basename(reqItem.preview_img),
					};
				}

				// update view images
				const newViewImgsBasenames = reqItem.view_imgs.map((url) => {
					return path.basename(url);
				});

				item.view_imgs = [];

				// remove old images
				for(const viewImg of dbResult.view_imgs) {
					if(newViewImgsBasenames.indexOf(viewImg.orig_basename) === -1) {						
						fs.unlink(cwd + viewImg.path, (err) => {
							if(err !== null)
								console.error(err);
						});
					}
					else
						item.view_imgs.push(viewImg);
				}

				const oldViewImgsBasenames = dbResult.view_imgs.map((viewImg) => {
					return viewImg.orig_basename;
				});

				// add new images
				for(const viewImgUrl of reqItem.view_imgs) {
					if(config.items.img_exts.indexOf(path.extname(viewImgUrl)) === -1)
						continue;

					const basename = path.basename(viewImgUrl);

					if(oldViewImgsBasenames.indexOf(basename) !== -1)
						continue;

					const randomPostfix = utils.genRandomString(10);

					itemImgs.push({
						path: `${cwd}/public/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
						url: viewImgUrl,
					});

					item.view_imgs.push({
						path: `/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
						orig_basename: basename,
					});
				}

				returnPromise = itemsCollection.updateOne(
					{
						_id: itemId,
					},

					{
						$set: item,
					},
				);
			}

			// download images
			for(const { url, path } of itemImgs) {
				request
				.get(url)
				.on('error', function(err) {
					console.log(err);
				})
				.pipe(fs.createWriteStream(path));
			}

			return returnPromise;
		})
		.then((res) => {
			return {
				success: true,
				origUrl: reqItem.orig_url,
			};
		})
		.catch((err) => {
			console.log('[ERROR] trying to upsert item', reqItem, err);

			return {
				success: false,
				origUrl: reqItem.orig_url,
				reason: 'database error',
			};
		});

		queryPromises.push(dbResultPromise);
	}

	Promise.all(queryPromises)
	.then((results) => {
		let success = true;
		let successItems = [];
		let failItems = [];

		results.forEach((result) => {
			if(result.success === false) {
				success = false;

				failItems.push({
					orig_url: result.origUrl,
					reason: result.reason,
				});
			}
			else
				successItems.push(result.origUrl);
		});

		res.json({
			success,
			success_items: successItems,
			fail_items: failItems,
		});
	})
	.catch((err) => {
		console.log('[ERROR] trying to analyze upserted items', err);

		res.json({
			success: false,
		});
	});
});

/*const itemsStorage = multer.diskStorage({
	destination: `${cwd}/public/img/items/`,

	filename: (req, file, cb) => {
		const itemId = new ObjectID();
		const itemHexId = itemId.toHexString();

		// const randomPostfix = utils.genRandomString(10);

		itemImgs.push({
			path: `${cwd}/public/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
			url: viewImgUrl,
		});

		item.view_imgs.push({
			path: `/public/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
			orig_basename: basename,
		});

		path: `${cwd}/public/img/items/${itemHexId}_main${path.extname(reqItem.preview_img)}`
	},
});*/

const itemUpload = multer({ storage: multer.memoryStorage() });

router.post(
	'/add_item',

	itemUpload.fields(
		[
			{
				name: 'preview_img',
				maxCount: 1,
			},

			{
				name: 'view_imgs',
				maxCount: 15,
			},
		],
	),

(req, res) => {
	console.log(req.files);
	console.log(req.body);

	const body = req.body;

	// TODO
	body.vendor = parseInt(body.vendor);
	body.category = parseInt(body.category);
	body.price = parseInt(body.price);
	body.specs = JSON.parse(body.specs);

	// console.log(body, body instanceof Object, typeof body);

	/*if(body instanceof Object === false)
		return res.status(400).end();*/

	// validate input
	/*if(
		Number.isInteger(body.price) === false
		|| body.price < 1
		|| typeof body.name !== 'string'
		|| Number.isInteger(body.vendor) === false
		|| Number.isInteger(body.category) === false
		|| body.specs instanceof Array === false
	)
		return res.status(400).end();
*/
	// console.log(body);
	// TODO: validate pictures
	
	const item = {
		price: body.price,
		name: body.name,
		vendor: body.vendor,
		category: body.category,
		specs: body.specs,
	};

	if(typeof body.orig_url === 'string')
		item.orig_url = body.orig_url;

	const itemId = new ObjectID();
	const itemHexId = itemId.toHexString();

	// change mongoDB _id
	item._id = itemId;

	// save preview img
	item.preview_img = {
		path: `/img/items/${itemHexId}_preview${path.extname(req.files.preview_img[0].originalname)}`,
		orig_basename: path.basename(req.files.preview_img[0].originalname),
	};

	fs.writeFile(
		`${cwd}/public/img/items/${itemHexId}_preview${path.extname(req.files.preview_img[0].originalname)}`,
		req.files.preview_img[0].buffer,
		
		(err) => {
			if(err !== null)
				console.log('[ERROR] libs/items/api - add_item method, preview img saving', req.files.preview_img, err);
		},
	);

	item.view_imgs = [];

	// save view images
	for(const viewImgFile of req.files.view_imgs) {
		const basename = path.basename(viewImgFile.originalname);
		const extname = path.extname(viewImgFile.originalname);

		const randomPostfix = utils.genRandomString(10);

		item.view_imgs.push({
			path: `/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
			orig_basename: basename,
		});

		fs.writeFile(
			`${cwd}/public/img/items/${itemHexId}_${randomPostfix}${path.extname(basename)}`,
			viewImgFile.buffer,
			
			(err) => {
				if(err !== null)
					console.log('[ERROR] libs/items/api - add_item method, view imgs saving', viewImgFile, err);
			},
		);
	}

	// insert item
	itemsCollection.insertOne(item)
	.then((result) => {
		if(result.insertedCount !== 1)
			return res.status(400).end();

		return res.json({
			id: result.insertedId,
		});
	})
	.catch((err) => {
		console.log('[ERROR] libs/items/api - add_item inserting item to db', item, err);
	});
});

/*
	body: {
		id:
		item params
		remove_view_imgs - json array, random postfixes
	}
 */
router.post(
	'/update_item',

	itemUpload.fields(
		[
			{
				name: 'preview_img',
				maxCount: 1,
			},

			{
				name: 'view_imgs',
				maxCount: 15,
			},
		],
	),

async (req, res) => {
	const body = req.body;

	/*if(body instanceof Object === false)
		return res.status(400).end();*/

	if(ObjectID.isValid(body.id) === false)
		return res.status(400).end();

	// get item
	const item = await itemsCollection.findOne({
		_id: ObjectID.createFromHexString(body.id),
	})
	.catch((err) => {
		console.log('[ERROR] libs/items/api - update_item, trying to get old item', body, err);

		return null;
	});

	if(item === null)
		return res.status(400).end();

	const updateOp = {
		$set: {},
		$pull: {},
		$push: {},
	};

	if(body.vendor !== void 0)
		updateOp.$set.vendor = parseInt(body.vendor)
	
	if(body.category !== void 0)
		updateOp.$set.category = parseInt(body.category)
	
	if(body.price !== void 0)
		updateOp.$set.price = parseInt(body.price)
	
	if(body.specs !== void 0)
		updateOp.$set.specs = JSON.parse(body.specs)

	if(body.name !== void 0)
		updateOp.$set.name = body.name;

	if(body.orig_url !== void 0)
		updateOp.$set.orig_url = body.orig_url;

	// update preview img
	if(req.files.preview_img !== void 0 && req.files.preview_img.length === 1) {
		// remove prev preview image
		new Promise((resolve, reject) => {
			fs.unlink(`${cwd}/public${item.preview_img.path}`, (err) => {
				if(err !== null)
					console.log('[ERROR] libs/items/api - update_item, trying to delete old preview_img', body, item, err);

				resolve(true);
			});
		})
		.then(() => {
			// save new file
			fs.writeFile(
				`${cwd}/public/img/items/${item._id}_preview${path.extname(req.files.preview_img[0].originalname)}`,
				req.files.preview_img[0].buffer,
				
				(err) => {
					if(err !== null)
						console.log('[ERROR] libs/items/api - update_item method, preview img saving', body, item, err);
				},
			);
		})
		.catch(console.log);
		
		updateOp.$set.preview_img = {
			path: `img/items/${item._id}_preview${path.extname(req.files.preview_img[0].originalname)}`,
			orig_basename: path.basename(req.files.preview_img[0].originalname),
		};
	}

	// remove view imgs
	if(req.body.remove_view_imgs !== void 0) {
		const toRemoveView = JSON.parse(req.body.remove_view_imgs);

		if(toRemoveView instanceof Array === true) {
			for(const viewImgPostfix of toRemoveView) {
				// search for this view image
				const viewImg = item.view_imgs.find((elem) => {
					const extname = path.extname(elem.path);
					const basename = path.basename(elem.path, extname);
					const postfix = basename.slice(-10);

					return viewImgPostfix === postfix;
				});

				// not found
				if(viewImg === void 0)
					continue;

				// remove image
				fs.unlink(`${cwd}/public${viewImg.path}`, (err) => {
					if(err !== null)
						console.log('[ERROR] libs/items/api - update_item, trying to delete view img', body, item, err, toRemoveView);
				});

				updateOp.$pull['view_imgs.path'] = viewImg.path;
			}
		}	
	}

	// add view imgs
	if(req.files.view_imgs !== void 0 && req.files.view_imgs.length > 0) {
		for(const viewImgFile of req.files.view_imgs) {
			const basename = path.basename(viewImgFile.originalname);
			const extname = path.extname(viewImgFile.originalname);

			const randomPostfix = utils.genRandomString(10);

			updateOp.$push.view_imgs = {
				path: `/img/items/${item._id}_${randomPostfix}${path.extname(basename)}`,
				orig_basename: basename,
			};

			// save file
			fs.writeFile(
				`${cwd}/public/img/items/${item._id}_${randomPostfix}${path.extname(basename)}`,
				viewImgFile.buffer,
				
				(err) => {
					if(err !== null)
						console.log('[ERROR] libs/items/api - update_item method, view imgs saving', viewImgFile, body, err);
				},
			);
		}
	}

	if(Object.keys(updateOp.$set).length === 0)
		delete updateOp.$set;

	if(Object.keys(updateOp.$pull).length === 0)
		delete updateOp.$pull;

	if(Object.keys(updateOp.$push).length === 0)
		delete updateOp.$push;

	// update
	itemsCollection.findOneAndUpdate(
		{
			_id: item._id,
		},

		updateOp,
	)
	.then((result) => {
		if(result.ok !== 1)
			return res.status(400).end();

		return res.json({
			success: true,
		});
	})
	.catch(console.log);
});

router.post('/get_items', (req, res) => {
	const body = req.body;

	if(body instanceof Object === false)
		return res.status(400).end();

	if(
		Number.isInteger(body.page) === false
		|| body.page < 0
		|| body.page >= 10000
	)
		body.page = 0;

	if(
		Number.isInteger(body.page_size) === false
		|| body.page_size <= 0
		|| body.page_size > 10
	)
		body.page_size = 5;

	itemsCollection.find({})
		.skip(body.page_size * body.page)
		.limit(body.page_size)
		.toArray()
		.then((docs) => {
			res.json(docs);
		})
		.catch((err) => {
			console.log('[ERROR]  libs/items/api - get_items', body, err);
		});
});