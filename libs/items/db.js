const dbConnection = require('../db_connection.js');
const config = require('../../config.json');

/**
 * DBMS notes:
 *  mongodb collection it's one category
 *  TODO: create index of categories
 *  
 */

let db;
let itemsCollection;

dbConnection.init()
.then(() => {
	db = dbConnection.client.db(config.db.main_db_name);
	itemsCollection = db.collection(config.items.collection_name);
});

exports.insertItems = (items) => {
	if(items instanceof Array === false)
		return Promise.reject('items is not instance of Array');

	return itemsCollection.insertMany(items);
};

exports.queryItems = (filter) => {
	if(filter instanceof Object === false)
		return;

	return itemsCollection.find(filter);
};

exports.queryItem = (filter, options = null) => {
	if(filter instanceof Object === false)
		return;

	return itemsCollection.findOne(filter, options);
};

/*
exports.updateItem = (categoryId, filter, update) => {
	if(filter instanceof Object === false)
		return;

	if(Number.isInteger(categoryId) === false)
		return;

	return itemsCollection.updateOne(filter, update);
};

exports.deleteItems = (categoryId, filter) => {
	if(filter instanceof Object === false)
		return;

	if(Number.isInteger(categoryId) === false)
		return;

	return itemsCollection.deleteMany(filter);
};*/

/*exports.setSpecsDoc = (doc) => {
	if(doc instanceof Object === false)
		return;

	return db.collection(settings.items.metadata_collection).replaceOne(
		{ _id: settings.items.specs_doc_id },
		doc,
	);
};*/
/*
exports.setSpec = (id, value) => {
	if(Number.isInteger(id) === false)
		return;

	if(typeof value !== 'string')
		return;

	return db.collection(settings.items.metadata_collection).updateOne(
		{ _id: settings.items.specs_doc_id },
		{
			$set: { [id]: value },
		},
	);
};

exports.setCategory = (id, value) => {
	if(Number.isInteger(id) === false)
		return;

	if(typeof value !== 'string')
		return;

	return db.collection(settings.items.metadata_collection).updateOne(
		{ _id: settings.items.categories_doc_id },
		{
			$set: { [id]: value },
		},
	);
};*/


