/*const dbConnection = require('./libs/db_connection.js');
const auth = require('./libs/users/db.js');

dbConnection.init()
.then(() => {
	return auth.authUser('viexus.team@gmail.com', 'testpass');
})
.then((result) => {
	console.log('result', result);
})
.catch(console.error);*/

/*const express = require('express');
const BSON = require('bson');
const bodyParser = require('body-parser');

const app = express();

app.use(express.static('public'));

app.use('/testreq', bodyParser.raw());

app.use('/testreq', (req, res, next) => {
	console.log(req.body);

	next();
});

app.listen(3000, () => {
	console.log(`Server is ready and listening port ${3000}`);
});*/

const request = require('request-promise-native');

request({
	uri: 'http://localhost:3000/api/items/upsert_items',
	json: true,
	method: 'POST',
	body: {
		items: [
			{
				price: 5200,
				name: 'Govno na palochke Хехе',
				vendor: 1,
				category: 2,
				orig_url: 'https://www.mvideo.ru/products/kronshtein-dlya-monitora-north-bayou-f160-black-50051413',
				pic_url: 'http://img.mvideo.ru/Pdb/50051413m.jpg',
				big_pic_urls: [ 
			        'http://img.mvideo.ru/Pdb/50051413b20.jpg',
				],

				specs: [[4, 'КНР'], [42, 'Настольный']],
			},

			{
				price: 3000,
				name: 'YOMA, MAM ZVONET',
				vendor: 1,
				category: 4,
				orig_url: 'https://www.mvideo.ru/products/kronshtein-dlya-monitora-north-bayou-f160-black-50051413',
				pic_url: 'http://img.mvideo.ru/Pdb/50051413b20.jpg',
				big_pic_urls: [ 
					'http://img.mvideo.ru/Pdb/50051413b20.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b21.jpg',
				],

				specs: [[4, 'КНР'], [42, 'Настольный']],
			}
		],
	}
})
.then(console.log)
.catch(console.log);

/*			        'http://img.mvideo.ru/Pdb/50051413b22.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b20.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b21.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b22.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b20.jpg',
			        'http://img.mvideo.ru/Pdb/50051413b21.jpg',*/

/*
const itemsDb = require('./libs/items/db.js');

setTimeout(() => {
	itemsDb.queryItem({})
	.then((a) => {
		console.log(Object.keys(a));
	});
}, 2000);
*/