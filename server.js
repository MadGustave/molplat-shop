const express = require('express');
const settings = require('./config.json');
const apiRouter = require('./libs/api.js');
const cookieParser = require('cookie-parser');
const authMiddleware = require('./libs/users/auth_middleware.js');
const mailConnection = require('./libs/mail_connection.js');
const dbConnection = require('./libs/db_connection.js');

const app = express();

// static files
app.use(express.static('public'));

// template engine
app.set('view engine', 'ejs');

app.use(cookieParser());

// use auth middleware for every request for now
app.use(authMiddleware);

/*FOR AUTH TEST*/
/*app.get('/test', (req, res) => {
	console.log(req.userId);
});
*/
// API
app.use('/api', apiRouter);

// init various connections and server
Promise.all([
	mailConnection.init(),
	dbConnection.init(),
])
.then(() => {
	const serverPort = process.env.PORT || 3000;

	app.listen(serverPort, () => {
		console.log(`Server is ready and listening port ${serverPort}`);
	});
})
.catch((err) => {
	console.error('[ERROR] Some error occured during server initialization', err);

	process.exit(1);
});